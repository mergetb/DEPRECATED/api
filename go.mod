module gitlab.com/mergetb/facility/api

go 1.15

require (
	github.com/golang/protobuf v1.5.2
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.4.0
	github.com/mwitkow/go-proto-validators v0.3.2
	github.com/sirupsen/logrus v1.8.1
	github.com/stormcat24/protodep v0.0.0-20210528163905-372c25740008
)

replace github.com/stormcat24/protodep => github.com/mergetb/protodep v0.0.0-20210611003340-401743f14904
